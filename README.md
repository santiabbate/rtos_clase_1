# Clase 1 y 2: Ejercicios con memoria dinámica. 

## Librería
```c
task_messages.h
```

### Dos tipos de datos
`task_messages_handler_t` Instancia del controlador de mensajes
`task_message_t` Tipo de mensajes que maneja la librería

### Comentarios

La librería maneja dos tipos de datos, uno para la instancia que controla la transmisión y recepción y otro tipo de dato genérico para los mensajes.

El tipo de dato genérico se inicializa con una función que crea un nuevo mensaje.
Ahora hay una sola función (`task_message_new_ascii()`) para crear mensajes, y crea mensajes de cadenas ascii.

Se puede instanciar más de un manejador de mensajes, y cada uno maneja la cantidad de mensajes que tiene. Todavía faltaría pulir un poco esa parte porque no hay detección hacia el usuario si el mensaje pertenece a un handler específico. (Por ejemplo, una tarea podría eliminar un mensaje que recibió a través de un handler, pero en realidad el mensaje fue creado desde otro handler). Ver comentario en la tarea c. (`tareas.c`)

En el programa ejemplo todavía no está modularizado el uso de teclas de la ciaa.
### Funciones

```c
/**
 * @brief Inicializa el handler de mensajes
 * 
 * @param handler Puntero al handler a iniciar
 * @param max_messages Cantidad máxima de mensajes que se admite
 * @return int -1 si hubo error en la creación del handler, 0 si estuvo todo OK
 */
int task_message_handler_init(task_messages_handler_t * handler, uint32_t max_messages); 

// Creación de mensajes según el tipo

/**
 * @brief Crea un mensaje 
 * 
 * @param handler Puntero al handler de mensajes
 * @param string Caracteres del mensajes
 * @param size Cantidad de caracteres
 * @return task_message_t* Puntero al nuevo mensaje alocado dinámicamente, si retorna NULL no pudo crear el mensaje
 */

task_message_t* task_message_new_ascii(task_messages_handler_t * handler, const char * string, uint32_t size);

/**
 * @brief Envía un mensaje previamente creado a través de un handler de mensajes
 * 
 * @param handler Puntero al handler de mensajes
 * @param msg Mensaje a enviar
 * @return int Retorna 0 si pudo mandar el mensaje, -1 si hubo error
 */
int task_message_send(task_messages_handler_t * handler, task_message_t* msg);

/**
 * @brief Recibe un mensaje del handler de mensajes
 * 
 * @param handler Puntero al handler de mensajes
 * @param msg Puntero a donde voy a guardar el mensaje
 * @return int Retorna 0 si pudo mandar el mensaje, -1 si hubo error
 */
int task_message_read(task_messages_handler_t * handler, task_message_t** msg);

void task_message_delete(task_messages_handler_t * handler, task_message_t * msg);
```
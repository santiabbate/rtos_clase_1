#ifndef TASK_MESSAGES_H
#define TASK_MESSAGES_H

#include "FreeRTOS.h"
#include "queue.h"


/**
 * Estructura de manejo del handler de mensajes
 */
typedef struct 
{
    uint32_t max_messages;  // Límite de mensajes
    uint32_t messages;      // Cantidad actual de mensajes        
    xQueueHandle queue;     // Cola de freeRTOS por la que se envían los mensajes    
}task_messages_handler_t;

typedef void* msg_t;


/**
 *  Tipo de datos de los mensajes que maneja la librería
 */
typedef struct
{
    uint32_t    num_bytes;  // Cantidad de bytes del mensaje
    msg_t   msg_pointer;    // Puntero al mensaje alocado dinámicamente    
}task_message_t;

/**
 * @brief Inicializa el handler de mensajes
 * 
 * @param handler Puntero al handler a iniciar
 * @param max_messages Cantidad máxima de mensajes que se admite
 * @return int -1 si hubo error en la creación del handler, 0 si estuvo todo OK
 */
int task_message_handler_init(task_messages_handler_t * handler, uint32_t max_messages); 

// Creación de mensajes según el tipo

/**
 * @brief Crea un mensaje 
 * 
 * @param handler Puntero al handler de mensajes
 * @param string Caracteres del mensajes
 * @param size Cantidad de caracteres
 * @return task_message_t* Puntero al nuevo mensaje alocado dinámicamente, si retorna NULL no pudo crear el mensaje
 */
task_message_t* task_message_new_ascii(task_messages_handler_t * handler, const char * string, uint32_t size);

/**
 * @brief Envía un mensaje previamente creado a través de un handler de mensajes
 * 
 * @param handler Puntero al handler de mensajes
 * @param msg Mensaje a enviar
 * @return int Retorna 0 si pudo mandar el mensaje, -1 si hubo error
 */
int task_message_send(task_messages_handler_t * handler, task_message_t* msg);

/**
 * @brief Recibe un mensaje del handler de mensajes
 * 
 * @param handler Puntero al handler de mensajes
 * @param msg Puntero a donde voy a guardar el mensaje
 * @return int Retorna 0 si pudo mandar el mensaje, -1 si hubo error
 */
int task_message_read(task_messages_handler_t * handler, task_message_t** msg);

void task_message_delete(task_messages_handler_t * handler, task_message_t * msg);





#endif  // TASK_MESSAGES_H
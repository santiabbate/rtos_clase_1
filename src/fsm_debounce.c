#include "FreeRTOS.h"
#include "queue.h"
#include "sapi.h"
#include "fsm_debounce.h"
#include "string.h"

#include "task_messages.h"

extern xQueueHandle cola;

extern task_messages_handler_t message_handler;

void mensaje_tecla(gpioMap_t tecla, portTickType pressedElapsedTime)
{
   task_message_t * my_message;

   // Armo el mensaje a mandar
   char mensaje[] = "TECx Tyyyy\n";
   sprintf(mensaje,"TECx T%04d\n",pressedElapsedTime);
   mensaje[3] = tecla == TEC1 ? (const char)'1' : (const char)'2';


   my_message = task_message_new_ascii(&message_handler, mensaje, sizeof(mensaje));

   if (NULL == my_message)
        {
            printf("Tarea b: No se pudo crear el mensaje\r\n");
        }
         // Envío el mensaje
        else 
        {
            if(0 < task_message_send(&message_handler, my_message)) // Envío el mensaje
            {
                  printf("Tarea b: No se pudo enviar el mensaje\r\n");
            }  
        }
}


void buttonPressed(  debouncedButton_t *button  )
{
   //Acá cuento inicio tiempo
   button->pressedTime = xTaskGetTickCount();
   // printf("PRESSED %d\r\n",button->name);
}

void buttonReleased( debouncedButton_t *button   )
{
   //Acá cuento fin tiempo
   button->pressedElapsedTime = xTaskGetTickCount() - button->pressedTime;
   mensaje_tecla(button->name,button->pressedElapsedTime);
}

// void fsmButtonError( void )
// {
//    fsmButtonState = BUTTON_UP;
// }

void fsmButtonInit( debouncedButton_t *button  )
{
   button->state = BUTTON_UP;  // Set initial state
   button->pressedTime = 0;
   button->pressedElapsedTime = 500;
   //button->name = TEC1;
   // printf("INIT\r\n");
}

// FSM Update Sate Function
void fsmButtonUpdate( debouncedButton_t *button )
{
   static uint8_t contFalling = 0;
   static uint8_t contRising = 0;
   
   switch( button->state ){

      case STATE_BUTTON_UP: 
         /* CHECK TRANSITION CONDITIONS */
         if( !gpioRead(button->name) ){
            button->state = STATE_BUTTON_FALLING;
         }
      break;

      case STATE_BUTTON_DOWN:
         /* CHECK TRANSITION CONDITIONS */
         if( gpioRead(button->name) ){
            button->state = STATE_BUTTON_RISING;
         }
      break;

      case STATE_BUTTON_FALLING:          
         /* CHECK TRANSITION CONDITIONS */
         if( contFalling >= 40 ){
            if( !gpioRead(button->name) ){
               button->state = STATE_BUTTON_DOWN;
               buttonPressed(button);
            } else{
               button->state = STATE_BUTTON_UP;
            }
            contFalling = 0;
         }
         contFalling++;
      break;

      case STATE_BUTTON_RISING:        
         /* CHECK TRANSITION CONDITIONS */
         
         if( contRising >= 40 ){
            if( gpioRead(button->name) ){
               button->state = STATE_BUTTON_UP;
               buttonReleased(button);
            } else{
               button->state = STATE_BUTTON_DOWN;
            }
            contRising = 0;
         }
         contRising++;
      break;

      default:
         //fsmButtonError();
      break;
   }
}

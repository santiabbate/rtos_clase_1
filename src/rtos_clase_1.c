/*==================[inlcusiones]============================================*/

// Includes de FreeRTOS
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "queue.h"
#include "sapi.h"

#include "fsm_debounce.h"
#include "tareas.h"

#include "task_messages.h"

/*==================[definiciones y macros]==================================*/
/*==================[definiciones de datos internos]=========================*/
// Creo teclas
debouncedButton_t tecla_1;
debouncedButton_t tecla_2;
teclas_t teclas;

// Handle del manejador de mensajes
task_messages_handler_t message_handler;

task_messages_handler_t extra_message_handler;

/*==================[definiciones de datos externos]=========================*/

/*==================[declaraciones de funciones internas]====================*/

/*==================[declaraciones de funciones externas]====================*/

/*==================[funcion principal]======================================*/

// FUNCION PRINCIPAL, PUNTO DE ENTRADA AL PROGRAMA LUEGO DE ENCENDIDO O RESET.
int main(void)
{
   int error = 0;
   BaseType_t xReturned;   
   // ---------- CONFIGURACIONES ------------------------------
   // Inicializar y configurar la plataforma
   boardConfig();

   // UART for debug messages
   uartInit( UART_USB, 115200 );

   // Inicializo parámetros de las tareas
   tecla_1.name = TEC1;
   tecla_2.name = TEC2;
   teclas.tecla_1 = &tecla_1;
   teclas.tecla_2 = &tecla_2;

   // Tarea A: Enciende periódicamente un LED y envía el mensaje "LED ON"
   xReturned = xTaskCreate(
               tarea_a,
               (const char *) "tarea_a",
               configMINIMAL_STACK_SIZE*2,
               NULL,
               tskIDLE_PRIORITY+1,
               0
               );

   if(xReturned == pdPASS){
	   printf("Tarea A OK\r\n");
   }
   else{
      error = -1;
   }

   // Tarea B: Maneja el antirrebote de las teclas, mide el tiempo de pulsación, y envía el mensaje "TECx yyyy"
   // Recibe como parámetro una estructura que tiene los punteros a las instancias de las teclas
   xReturned = xTaskCreate(
               tarea_b,                     
               (const char *)"tarea_b",     
               configMINIMAL_STACK_SIZE*2, 
               &teclas,                    
               tskIDLE_PRIORITY+1,         
               0                           
               );

   if(xReturned == pdPASS){
	   printf("Tarea B OK\r\n");
   }
   else{
      error = -1;
   }

   // Tarea C: Recibe mensajes de las tareas A y B
   xReturned =  xTaskCreate(
      tarea_c,                     
      (const char *)"tarea_c",     
      configMINIMAL_STACK_SIZE*2, 
      NULL,                        
      tskIDLE_PRIORITY+1,         
      0                           
      );

   if(xReturned == pdPASS){
	   printf("Tarea C OK\r\n");
   }
   else{
      error = -1;
   }

   // Tarea C: Recibe mensajes de las tareas A y B
   xReturned = xTaskCreate(
               tarea_d,                     
               (const char *)"tarea_d",     
               configMINIMAL_STACK_SIZE*2, 
               NULL,                        
               tskIDLE_PRIORITY+1,         
               0                           
               );

   if(xReturned == pdPASS){
	   printf("Tarea D OK\r\n");
   }
   else{
      error = -1;
   }

   // Inicializo instancia al primer controlador de mensajes
   if (0 > task_message_handler_init(&message_handler, 10))
   {
      error = -1;   
   }
    

   // Inicializo instancia al segundo controlador de mensajes
   if (0 > task_message_handler_init(&extra_message_handler, 20))
   {
      error = -1;   
   }

   // Si todo estuvo bien, arranco el scheduler
   if(0 == error){
      vTaskStartScheduler(); // Initialize scheduler
   }
   else{
      printf("Error iniciando el sistema\r\n");
   }

   // ---------- REPETIR POR SIEMPRE --------------------------
   while( TRUE ) {
      // Si cae en este while 1 significa que no pudo iniciar el scheduler
   }
   return 0;
}

/*==================[definiciones de funciones internas]=====================*/

/*==================[definiciones de funciones externas]=====================*/

/*==================[fin del archivo]========================================*/

#include "FreeRTOS.h"
#include "queue.h"
#include "sapi.h"   
#include "fsm_debounce.h"
#include "tareas.h"
#include "string.h"

#include "task_messages.h"

extern task_messages_handler_t message_handler;
extern task_messages_handler_t extra_message_handler;

// Parpadea un led cada 1 segundo y envía "LED ON"
void tarea_a( void* taskParmPtr )
{
    // Tarea periodica cada 1000 ms
    portTickType xPeriodicity =  1000 / portTICK_RATE_MS;
    portTickType xLastWakeTime = xTaskGetTickCount();
    const char mensaje[] = "LED ON\n"; // Mensaje a enviar, de 7 caracteres

    task_message_t * my_message;  

    // ---------- REPETIR POR SIEMPRE --------------------------
    while(TRUE) {
        gpioToggle( LED1 );

        // Creo el mensaje
        
        my_message = task_message_new_ascii(&message_handler, mensaje, sizeof(mensaje));

        if (NULL == my_message)
        {
            printf("Tarea a: No se pudo crear el mensaje\r\n");
        }
        else
        {
            if(0 < task_message_send(&message_handler, my_message)) // Envío el mensaje
            {
                printf("Tarea a: No se pudo enviar el mensaje\r\n");
            }
        }
              
        vTaskDelayUntil( &xLastWakeTime, xPeriodicity );
    }
}


// Debounce de cada tecla instanciada (en este caso 2) y envío de mensaje (lo hace dentro de máquina de estados)
void tarea_b(void* taskParmPtr )
{
    teclas_t *teclas = (teclas_t *)taskParmPtr;
    fsmButtonInit(teclas->tecla_1);
    fsmButtonInit(teclas->tecla_2);

    while (TRUE)
    {
        fsmButtonUpdate(teclas->tecla_1);
        fsmButtonUpdate(teclas->tecla_2);
        vTaskDelay( 1 / portTICK_RATE_MS);
    }
}

// Recibe los mensajes, los imprime, y libera la memoria alocada
void tarea_c( void* taskParmPtr )
{
    task_message_t * my_message_received;
    task_message_t * my_message_to_send;

    while(TRUE) {

        if (0 == task_message_read(&message_handler, &my_message_received))
        {
            printf("Mensaje: %s", my_message_received->msg_pointer);

            /**
             * Esta sección podría resolverse mejor, porque estoy
             * re alocando memoria para una copia del mensaje,
             * pero podría tomar el mensaje ya creado y reenviarlo
             * a la otra tarea. Por ahora lo resuelvo de esta manera
             * para que cada tarea se encargue de eliminar los mensajes
             * que recibe del handler, y que no vayan pasando de tarea en tarea
             * 
             */

            // Creo un nuevo mensaje ascii usando como string el mensaje recibido
            my_message_to_send = task_message_new_ascii(&extra_message_handler,(const char *) my_message_received->msg_pointer, my_message_received->num_bytes);

            if (NULL == my_message_to_send)
            {
                printf("Tarea c: No se pudo crear el mensaje\r\n");
            }
            else {
                // Re-envío el mensaje a la tarea d
                if(0 < task_message_send(&extra_message_handler, my_message_to_send))
                {
                    printf("Tarea c: No se pudo reenviar el mensaje\r\n");
                }
            }

            // Borro el mensaje original
            task_message_delete(&message_handler, my_message_received);
        }
   }
}

// Recibe los mensajes extra de la tarea c, los imprime, y libera la memoria alocada
void tarea_d( void* taskParmPtr )
{
    task_message_t * my_message_received;

    while(TRUE) {

        if (0 == task_message_read(&extra_message_handler, &my_message_received))
        {
            printf("Mensaje REENVIADO: %s", my_message_received->msg_pointer);

            // Borro el mensaje recibido    
            task_message_delete(&extra_message_handler, my_message_received);        
        }
   }
}


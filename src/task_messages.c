#include "task_messages.h"
#include "string.h"


int size_available(task_messages_handler_t * handler)
{
    return (handler->messages < handler->max_messages);
}

task_message_t* task_message_new_ascii(task_messages_handler_t * handler, const char * string, uint32_t size){
    
    task_message_t * allocated_msg = NULL;
    if (size_available(handler))
    {
        // Aloco memoria para el tamaño de la cadena de caracteres ascii, y para la estructura que contiene el mensaje
        if(NULL != (allocated_msg = (task_message_t *) pvPortMalloc(size + sizeof(task_message_t)))) // Copio el mensaje ascii si se pudo alocar memoria
        {
            allocated_msg->num_bytes = size;
            allocated_msg->msg_pointer = (void*) allocated_msg + sizeof(task_message_t); // Guardo en la estructra del mensaje el puntero a los datos
            memcpy(allocated_msg->msg_pointer, string, size);   // Copio el mensaje
        }

        handler->messages = handler->messages + 1;
    }
    return(allocated_msg);
}

int task_message_handler_init(task_messages_handler_t * handler, uint32_t max_messages) 
{
    int error = 0;
    if (NULL == (handler->queue = xQueueCreate(max_messages,sizeof(void *))))
    {
        error = -1;
    }
    handler->max_messages = max_messages;
    handler->messages = 0;
    return error;
}

int task_message_send(task_messages_handler_t * handler, task_message_t* msg)
{   
    int error = 0;
    if (handler != NULL && msg != NULL)     // handler iniciado y es válido el mensaje
    {
        if(pdTRUE != xQueueSend(handler->queue, &msg, portMAX_DELAY)) // Envío por cola de freeRTOS
        {
            error = -1;
        }      
    }
    else
    {
        error = -1;
    }
    return (error);
}

int task_message_read(task_messages_handler_t * handler, task_message_t** msg)
{
	int error = 0;

    if (pdTRUE != xQueueReceive(handler->queue, msg, portMAX_DELAY))
    {
        error = -1;
    }
	return error;
}

void task_message_delete(task_messages_handler_t * handler, task_message_t * msg)
{
    vPortFree((void*) msg);

    if (handler->messages > 0)
    {
     handler->messages = handler->messages - 1;     
    }
}        
